Author : Noémie Kerroux [www.noemiekerroux.fr](www.noemiekerroux.fr)

# 1/ WHAT IS IT ?

This is a Wordpress Global research function adapted for this specific Wordpress theme :
https://github.com/opsone/wordpress-theme-kit

# 2/ HOW TO USE IT ?

In the folder 'modules', create a folder 'helpers', and put the 'search.php' file into it.
That's it ! You can use the researcher now.

Here is an exemple of your Search controller /modules/search/controller.php :

```
<?php
namespace Search;

use Controller\Base;
use WPSearch\WPGlobalSearch;

class SearchController extends Base
{
    protected function get()
    {
        global $term;
        global $wpdb;

        $query = (!empty($_GET['keywords'])) ? trim(strip_tags($_GET['keywords'])) : '';
        $page = (!empty($_GET['page'])) ? trim(strip_tags($_GET['page'])) : 1;
        $taxonomies = (!empty($_GET['type']) && !in_array($_GET['type'], ['none','all'])) ? ['type' => [strip_tags($_GET['type'])]] : [];

        $args = [
            'taxonomies' => $taxonomies
        ];

        $results = \WPSearch\WPGlobalSearch::wp_global_search($query, $page, $args);

        return $results;
    }
}
?>
```

# 3/ DOCUMENTATION

```
wp_global_search($query, $page, $args)
```

## argument $query
* [string] : query of the research (default : '')

## argument $page
* [int] : Page to display (default : 1)

## argument $args
* **accepted_post_types** [array of string] : *List of accepted posts (default : ['post','page'])*
* **posts_per_page** [int] : *Number of posts per page (default : 12)*
* **forbidden_templates** [array of string] : *Page to block from the research*
* **search_if_empty** [bool] : *Will return default posts with these arguments if the query is empty, otherwise the results will be empty.*
* **taxonomies** [array of array of strings] : *List of filtered taxonomies slugs (default: [] / Exemple : ['category' => ['wordpress','search']])*
* **multiple_words** [bool] : *Filter by multiple words (break at space) (default: true)*
* **post_metas** [array of strings] : *Array of post_metas to filter on*
* **search_on** [array of array of strings] : *Array of post attributs to filter on*


Contact [noemie.kerroux@gmail.com](mailto:noemie.kerroux@gmail.com) for further informations.