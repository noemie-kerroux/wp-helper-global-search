<?php

namespace WPSearch;

class WPGlobalSearch {
    public static function wp_global_search($query = 'déménagement', $page = 1, $args = []){

        global $wpdb;
        $errors  = [];
        $db_query = null;

        $default_args = [
            'accepted_post_types' => (!empty($args['accepted_post_types'])) ? $args['accepted_post_types'] : [
                'post',
                'page'
            ],
            'forbidden_templates' => (!empty($args['forbidden_templates'])) ? $args['forbidden_templates'] : [
                'templates/index.php',
                'templates/home.php',
                'templates/search.php',
            ],
            'posts_per_page' => (!empty($args['posts_per_page'])) ? $args['posts_per_page'] : 12,
            'search_if_empty' => (isset($args['search_if_empty'])) ? $args['search_if_empty'] : true,
            'multiple_words' => (isset($args['multiple_words'])) ? $args['multiple_words'] : true,
            'taxonomies' => (!empty($args['taxonomies'])) ? $args['taxonomies'] : [],
            'post_metas' =>  (!empty($args['post_metas'])) ? $args['post_metas'] : [],
            'search_on' => [
                'post_title' => ( (!empty($args['search_on']) && isset($args['search_on']['post_title']) ) ? $args['search_on']['post_title'] : true ),
                'post_content' => ( (!empty($args['search_on']) && isset($args['search_on']['post_content'])) ? $args['search_on']['post_content'] : true ),
                'post_metas' => ( (!empty($args['search_on']) && isset($args['search_on']['post_metas'])) ? $args['search_on']['post_metas'] : true ),
                'post_tags' => ( (!empty($args['search_on']) && isset($args['search_on']['post_tags'])) ? $args['search_on']['post_tags'] : true ),
            ],
        ];

        $articles = [];
        $count_articles = 0;
        $count_pages = 0;
        $last_page = 0;
        if($default_args['multiple_words']) {
            $query_array = (!empty($query)) ? explode(' ', $query) :  [];
        }else {
            $query_array = array($query);
        }

        $page = (!empty($_GET['page'])) ? trim(strip_tags($_GET['page'])) : 1;

        $post_title_likes = "";
        $post_content_likes = "";
        $t_name_likes = "";
        $post_metas_likes = "";
        $post_metas_keys = self::get_formatted_sql_element($default_args['post_metas']);
        $array_keys = array_keys($query_array);
        $last_key = end($array_keys);
        foreach ($query_array as $key => $q) {
            if($default_args['search_on']['post_title']){ $post_title_likes .= "p.post_title LIKE '%" . $q . "%' "; }
            if($default_args['search_on']['post_content']){ $post_content_likes .= "p.post_content LIKE '%" . $q . "%' "; }
            $t_name_likes .= "t.name LIKE '%" . $q . "%' ";
            if($default_args['search_on']['post_metas']){
                $post_metas_likes .= "pm.meta_value LIKE '%" . $q . "%' ";
            }
            if($key != $last_key){
                if($default_args['search_on']['post_title']){ $post_title_likes .= "OR "; }
                if($default_args['search_on']['post_content']){ $post_content_likes .= "OR "; }
                $t_name_likes .= "OR ";
                if($default_args['search_on']['post_metas']){ $post_metas_likes .= "OR "; }
            }
        }
        if($default_args['search_on']['post_metas'] && !empty($default_args['post_metas'])){
            $post_metas_likes .= " AND pm.meta_key IN " . $post_metas_keys . " ";
        }

        if( ( !empty($query) ) || ( $default_args['search_if_empty'] == true ) ){
            $offset = ($page - 1) * $default_args['posts_per_page'];

            $db_query = "SELECT SQL_CALC_FOUND_ROWS DISTINCT(p.ID), p.* FROM {$wpdb->base_prefix}posts p
            LEFT JOIN {$wpdb->base_prefix}term_relationships tr ON tr.object_id LIKE p.ID ";
            if(!empty($default_args['taxonomies']) || !$default_args['search_on']['post_tags']){
                $db_query .= "LEFT JOIN {$wpdb->base_prefix}term_taxonomy tt ON tt.term_id LIKE tr.term_taxonomy_id ";
            }
            $db_query .= "LEFT JOIN {$wpdb->base_prefix}terms t ON tr.term_taxonomy_id LIKE t.term_id
            LEFT JOIN {$wpdb->base_prefix}postmeta pm ON pm.post_id = p.ID
            WHERE p.post_type IN " . self::get_formatted_sql_element($default_args['accepted_post_types']) . "
            AND p.post_status LIKE 'publish'";

            if(!empty($default_args['taxonomies'])){
                $q_tax_parents = [];
                $q_tax_children = [];
                foreach ($default_args['taxonomies'] as $key => $taxonomy) {
                    $q_tax_parents[] = $key;
                    foreach ($taxonomy as $key => $term) {
                        $q_tax_children[] = $term;
                    }
                }
                $q_tax_parents = self::get_formatted_sql_element($q_tax_parents);
                $q_tax_children = self::get_formatted_sql_element($q_tax_children);
                $db_query .= " AND tt.taxonomy IN " . $q_tax_parents;
                $db_query .= " AND t.slug IN " . $q_tax_children;
            }
            if(!empty($query)){
                $db_query .= "AND (
                    (
                        " . ( (!empty($post_title_likes) || !empty($post_content_likes)) ? "(" : "" ) . "
                            " . ( !empty($post_title_likes) ? $post_title_likes . (!empty($post_content_likes) ? " OR " : "" ) : "" ) . ( !empty($post_content_likes) ? $post_content_likes : "" ) . "
                        " . ( (!empty($post_title_likes) || !empty($post_content_likes)) ? ") AND (" : "" ) . "



                            (p.post_type LIKE 'page'
                            AND pm.meta_key LIKE '_wp_page_template'
                            AND pm.meta_value NOT IN " . self::get_formatted_sql_element($default_args['forbidden_templates']) . ")
                            OR
                            (p.post_type NOT LIKE 'page')
                        " . ( (!empty($post_title_likes) || !empty($post_content_likes)) ? ")" : "" ) . "
                    )
                    OR
                    (
                        " . $t_name_likes . "
                    )
                    OR
                    (
                        (" . $post_metas_likes . ")
                        AND
                        pm.meta_key NOT LIKE '_wp_page_template'
                        AND
                        pm.post_id NOT IN " . self::get_formatted_sql_element_ids($default_args['forbidden_templates']) . "

                    )
                )";
            }

            if(!$default_args['search_on']['post_tags']){
                $db_query .= " AND tt.taxonomy NOT LIKE 'post_tag' ";
            }
            $db_query .= "ORDER BY p.post_date DESC
            LIMIT " . $default_args['posts_per_page'] . " OFFSET " . $offset . ";";

            $articles = $wpdb->get_results($db_query);
            $count_articles = $wpdb->get_var("SELECT FOUND_ROWS()");
            $count_pages = ceil($count_articles / $default_args['posts_per_page']);
            $last_page = ($count_pages == $page);
        }

        return array(
            'errors'         => $errors,
            'results'        => $articles,
            'db_query'       => $db_query,
            'query'          => $query,
            'args'           => $default_args,
            'page_number'           => $page,
            'count_articles' => $count_articles,
            'count_pages'    => $count_pages,
            'last_page'      => $last_page,
        );
    }

    public static function get_formatted_sql_element($array = []){
        $results = "(";
        foreach ($array as $key => $apt) {
            $results .= "'" . $apt . "'";
            if($apt !== end($array)){
                $results .= ',';
            }
        }
        $results .= ")";
        return $results;
    }

    public static function get_formatted_sql_element_ids($array = []){
        $results = "(";
        foreach ($array as $key => $apt) {
            $post_id = self::wp_search_get_template_id($apt);
            if(!empty($post_id)) {
                $results .= "'" . $post_id . "'";
                if($apt !== end($array)){
                    $results .= ',';
                }
            }
        }
        $results .= ")";
        return $results;
    }

    public static function wp_search_get_template_id($template_string)
    {
        // $template_string - Ex : template-products.php
        global $wpdb;
        $template_id = $wpdb->get_var("SELECT pm.post_id FROM {$wpdb->base_prefix}postmeta pm, {$wpdb->base_prefix}posts p WHERE pm.meta_key = '_wp_page_template' AND pm.meta_value = '" . $template_string . "' AND p.post_status = 'publish' AND p.ID = pm.post_id");
        return $template_id;
    }
}

?>
